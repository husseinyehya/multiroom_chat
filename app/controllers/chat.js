module.exports.iniciaChat = function (application, req, res){
	
	 var dadosForm = req.body;

	 console.log(dadosForm);

	 req.assert('apelido','Precisa preencher o campo nome').notEmpty();
	 req.assert('apelido','O minimo de caracter é de 3 até 15').len(3,15);

	 var erros= req.validationErrors();

	 if(erros){
	 	res.render("index", {validacao : erros })
	 	return;
	 }

	 res.render('chat.ejs');
}