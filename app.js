// importar as configurações do servidor
var app = require('./config/server');

// parametrizar a porta de escuta
var server = app.listen(80, function () {
	 console.log('Servidor ON');
}); 

var io = require('socket.io').listen(server);

//criar a conexão por websocket
io.on('connection', function(socket){
	 console.log('Usuário conectou');

	 socket.on('disconnect', function () {
	 	 console.log('Usuario deslogou');
	 })
});
